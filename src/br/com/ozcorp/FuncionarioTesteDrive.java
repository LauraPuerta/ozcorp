package br.com.ozcorp;

public class FuncionarioTesteDrive {

	public static void main(String[] args) {

		// diretor ok
		Diretor laura = new Diretor("Laura", "5327849658", "46940253840", "417854", "lalasp123sud@gmail.com",
				"xuxu", Sexo.FEMININO, TipoSanguineo.O_NEGATIVO, 0,
				new Departamento("Financeiro", "ABC", new Cargo("Diretora Financeiro", 10000)));

		System.out.println("Dados do funcionario:");
		System.out.println("Nome: " + laura.getNome());
		System.out.println("RG: " + laura.getRg());
		System.out.println("CPF: " + laura.getCpf());
		System.out.println("Matricula: " + laura.getMatricula());
		System.out.println("Email: " + laura.getEmail());
		System.out.println("Senha: " + laura.getSenha());
		System.out.println("Sexo:  " + laura.getSexo());
		System.out.println("Tipo sanguineo: " + laura.getTipoSanguineo());
		System.out.println("Cargo: " + laura.getDepartamento().getCargo().getTitulo());
		System.out.println("Salario base: " + laura.getDepartamento().getCargo().getSalarioBase());
		System.out.println();
		// secretario ok
		Secretario fernando = new Secretario("Fernando", "3742392359", "76234726928", "276205", "fernandin@gmail.com",
				"123", Sexo.MASCULINO, TipoSanguineo.AB_NEGATIVO, 1,
				new Departamento("Financeiro", "ABC", new Cargo("Secretario Financeiro", 4000)));

		System.out.println("Dados do funcionario:");
		System.out.println("Nome: " + fernando.getNome());
		System.out.println("RG: " + fernando.getRg());
		System.out.println("CPF: " + fernando.getCpf());
		System.out.println("Matricula: " + fernando.getMatricula());
		System.out.println("Email: " + fernando.getEmail());
		System.out.println("Senha: " + fernando.getSenha());
		System.out.println("Sexo:  " + fernando.getSexo());
		System.out.println("Tipo sanguineo: " + fernando.getTipoSanguineo());
		System.out.println("Cargo: " + fernando.getDepartamento().getCargo().getTitulo());
		System.out.println("Salario base: " + fernando.getDepartamento().getCargo().getSalarioBase());
		System.out.println();

		// gerente
		Gerente bianca = new Gerente("Bianca", "4786532594", "547846645", "487596", "bianca@gmail.com",
				"ola", Sexo.FEMININO, TipoSanguineo.A_POSITIVO, 2,
				new Departamento("Financeiro", "ABC", new Cargo("Gerente Financeiro", 5000)));

		System.out.println("Dados do funcionario:");
		System.out.println("Nome: " + bianca.getNome());
		System.out.println("RG: " + bianca.getRg());
		System.out.println("CPF: " + bianca.getCpf());
		System.out.println("Matricula: " + bianca.getMatricula());
		System.out.println("Email: " + bianca.getEmail());
		System.out.println("Senha: " + bianca.getSenha());
		System.out.println("Sexo:  " + bianca.getSexo());
		System.out.println("Tipo sanguineo: " + bianca.getTipoSanguineo());
		System.out.println("Cargo: " + bianca.getDepartamento().getCargo().getTitulo());
		System.out.println("Salario base: " + bianca.getDepartamento().getCargo().getSalarioBase());
		System.out.println();

		// engenheiro
		Engenheiro luiz = new Engenheiro("Luiz", "5467282453", "125415542", "369854", "luizin@gmail.com",
				"456", Sexo.MASCULINO, TipoSanguineo.B_NEGATIVO, 3,
				new Departamento("Financeiro", "ABC", new Cargo("Engenheiro", 8000)));

		System.out.println("Dados do funcionario:");
		System.out.println("Nome: " + luiz.getNome());
		System.out.println("RG: " + luiz.getRg());
		System.out.println("CPF: " + luiz.getCpf());
		System.out.println("Matricula: " + luiz.getMatricula());
		System.out.println("Email: " + luiz.getEmail());
		System.out.println("Senha: " + luiz.getSenha());
		System.out.println("Sexo:  " + luiz.getSexo());
		System.out.println("Tipo sanguineo: " + luiz.getTipoSanguineo());
		System.out.println("Cargo: " + luiz.getDepartamento().getCargo().getTitulo());
		System.out.println("Salario base: " + luiz.getDepartamento().getCargo().getSalarioBase());
		System.out.println();

		// analista ok
		Analista joao = new Analista("Jo�o", "6872957384", "96841257365", "698523", "jaum@gmail.com", "abc",
				Sexo.MASCULINO, TipoSanguineo.A_NEGATIVO, 4,
				new Departamento("Financeiro", "ABC", new Cargo("Analista Financeiro", 7000)));

		System.out.println("Dados do funcionario:");
		System.out.println("Nome: " + joao.getNome());
		System.out.println("RG: " + joao.getRg());
		System.out.println("CPF: " + joao.getCpf());
		System.out.println("Matricula: " + joao.getMatricula());
		System.out.println("Email: " + joao.getEmail());
		System.out.println("Senha: " + joao.getSenha());
		System.out.println("Sexo:  " + joao.getSexo());
		System.out.println("Tipo sanguineo: " + joao.getTipoSanguineo());
		System.out.println("Cargo: " + joao.getDepartamento().getCargo().getTitulo());
		System.out.println("Salario base: " + joao.getDepartamento().getCargo().getSalarioBase());
		System.out.println();

	}

}