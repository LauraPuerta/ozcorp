package br.com.ozcorp;

public enum Sexo {

	MASCULINO("masculino"), FEMININO("feminino"), OUTRO("outro");
	
	String nome;
	Sexo(String nome){
		this.nome = nome;
	}

}
